﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamTest02
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<string> contactList;

        public MainPage()
        {
            InitializeComponent();

            contactList = new ObservableCollection<string>();
            ListViewContactList.ItemsSource = contactList;
            
        }

        private void ButtonAdd_Clicked(object sender, EventArgs e)
        {
            LabelMessage.Text = "";
            if (string.IsNullOrEmpty(EntryName.Text))
            {
                LabelMessage.Text = "Write a contact name";
                return;
            }

            contactList.Add(EntryName.Text);
            LabelMessage.Text = EntryName.Text + " added";
            EntryName.Text = "";
        }
    }
}
